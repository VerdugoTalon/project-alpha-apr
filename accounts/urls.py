from django.urls import path
from .views import login_attempt, logout_view, signup_view

urlpatterns = [
    path("signup/", signup_view, name="signup"),
    path("logout/", logout_view, name="logout"),
    path("login/", login_attempt, name="login"),
]
