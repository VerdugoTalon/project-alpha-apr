from django.urls import path
from . import views

urlpatterns = [
    path("task/<int:task_id>/add_notes/", views.add_notes, name='add_notes'),
    path("mine/", views.show_my_tasks, name="show_my_tasks"),
    path("create/", views.create_task, name="create_task"),
]
