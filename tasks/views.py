from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .forms import TaskForm, TaskNotesForm
from .models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("project_list")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/my_tasks.html", context)

@login_required
def add_notes(request, task_id):
    task = get_object_or_404(Task, id=task_id, assignee=request.user)
    if request.method == 'POST':
        form = TaskNotesForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect('show_my_tasks', task_id=task.id)
    else:
        form = TaskNotesForm(instance=task)
    return render(request, 'add_notes.html', {'form': form, 'task': task})
