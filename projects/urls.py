from django.urls import path
from . import views

urlpatterns = [
    path("project/<int:project_id>/timeline/", views.project_timeline, name='project_timeline'),
    path("create/", views.create_project, name="create_project"),
    path("<int:id>/", views.show_project, name="show_project"),
    path("", views.show_project_list, name="list_projects"),
    path("", views.project_list, name="project_list"),
]
