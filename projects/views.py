from django.shortcuts import render, redirect, get_object_or_404
from .models import Project
from tasks.models import Task
from .forms import ProjectForm
from django.contrib.auth.decorators import login_required
import plotly.express as px
import pandas as pd


@login_required
def show_project_list(request):
    project_list = Project.objects.filter(owner=request.user)
    context = {"project_list": project_list}
    return render(request, "projects/project_list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    context = {
        "project": project,
        "tasks": tasks,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("project_list")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }

    return render(request, "projects/create_project.html", context)


def project_list(request):
    projects = Project.objects.all()
    return render(request, "projects/list.html", {"projects": projects})

@login_required
def project_timeline(request, project_id):
    project = get_object_or_404(Project, id=project_id)
    tasks = Task.objects.filter(project=project)
    df = pd.DataFrame(list(tasks.values('name', 'start_date', 'due_date', 'is_completed')))
    fig = px.timeline(df, x_start="start_date", x_end="due_date", y="name", color="is_completed", title="Project Timeline")
    gantt_chart = fig.to_html()
    
    return render(request, 'projects/project_timeline.html', {'gantt_chart': gantt_chart, 'project': project})